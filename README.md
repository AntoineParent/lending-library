# Lending Libray
Voir le protoype :
https://www.figma.com/proto/jE52PmdGGkRlKDIpmlbuby/LendingLibrary?node-id=1%3A2&scaling=scale-down

## Description du projet
### Concept :
L’application permet à ses utilisateurs de partager les livres qu’ils possèdent. C’est un système de prêt entre particulier. L’utilisateur peut faire une recherche sur un livre qu’il souhaite lire et voir les exemplaires détenus par les autres utilisateurs, et soumettre ainsi une demande de prêt si l’exemplaire est disponible. L’application devrait permettre au détenteur de gérer le statut de ses livres (disponible/déjà loué) et de mettre en contact via un système de messagerie le demandeur et le préteur afin qu’ils se mettent d’accord pour le prêt et la restitution.<br>
Cependant, une nouvelle orientation pour l’application a émergé après discussion avec Jean-Christophe. Le concept évolue un peu. Le propriétaire lorsqu’il ajoute un exemplaire à sa bibliothèque fait le choix de se “débarrasser” de son livre (comme les boîtes à livres). Le demandeur devient ainsi le nouveau propriétaire et ainsi de suite. L’exemplaire a donc plusieurs propriétaires au long de son existence, et voyage ainsi de main en main. Les utilisateurs pourront suivre le parcours d’un livre, son historique. L’application alimente ainsi une base de données de livres “voyageurs”.

### Idées de nom “temporaire” pour le projet :
* Livres libres
* Bibliothèques partagées en ligne
* Books next door
* Books corner
* Lending Library
* Partage de livres / Livres partagés
* Culture partagée, connaissances partagées / ...
* Cercle/Club de lecture
* Nouvelle orientation : le livre voyageur

## User stories
* L’utilisateur peut rechercher un livre soit par titre, isbn, auteur, genre...
* L’application propose une liste de résultat correspondants.
* L’utilisateur accède à une fiche détaillée du livre en cliquant sur un résultat.
* Si l’utilisateur n’est pas connecté, l’application lui propose de saisir ses identifiants ou de créer son compte.
* Dans la fiche détaillée d’un livre, l’utilisateur peut accéder à la liste des exemplaires.
* Un utilisateur non-inscrit peut accéder aux fonctionnalités de recherche mais sans voir la liste des exemplaires
* L’utilisateur peut choisir un exemplaire en fonction de sa localisation et faire une demande de partage.
* En cliquant sur “demande de partage”, un système de messagerie s’ouvre entre le demandeur et le détenteur.
* Un utilisateur peut noter / rédiger un avis sur un livre.
* Au moment de la transaction, détenteur et/ou demandeur devront actualiser le statut de l’exemplaire.
* Tous les utilisateur peuvent consulter le profil d’un membre et les informations associés (bibliothèque, avis...)
* Pour créer un compte, l’utilisateur doit saisir une adresse email pour lancer le processus (authentification à définir)

* L’utilisateur peut éditer son profil avec les informations suivantes :<br>
**Obligatoires :**
nom d’utilisateur, adresse mail, mot de passe, localisation<br>
**Facultatives :**
Genres de littérature préférés, biographie, photo/avatar, liste de souhaits…

* L’utilisateur peut saisir les titres qu’il possèdent / qu’ils souhaitent partager avec la communauté.
* Pour saisir un nouveau livre, l’utilisateur peut saisir son numéro ISBN (ou le scanner), son titre, son auteur
* L’application utilisera différentes APIs pour vérifier les informations saisies et les compléter si nécessaire. 

***Pour le futur :***
* L’utilisateur a accès à un tuto / présentation / aide
* L’utilisateur peut effectuer une recherche avancée
* L’utilisateur peut s’inscrire avec son compte google / facebook…

## Benchmark :
### Dorine : 
http://www.webibli.fr/ (juste un site pas d’appli - livres spécialisés dans les technologies web)<br>
http://classbook.fr/ (gestion de sa bibliothèque)

### Antoine :
https://www.allovoisins.com/r/0/55/0/0/location-vente/Livres<br>
https://www.bookyourbooks.com/fr/index.php<br>
http://www.booxup.com/<br>
http://bookmooch.com/<br>

### Andrès :
https://sourceforge.net/p/lendinglibrary/code/ci/master/tree/JLL/<br>
https://sourceforge.net/projects/booksell/<br>

# Technologies :
### API livres :
https://openlibrary.org/dev/docs/api/books<br>
https://developers.google.com/books/docs/v1/using<br>
https://www.worldcat.org/affiliate/tools?atype=wcapi<br>
https://docs.aws.amazon.com/fr_fr/AWSECommerceService/latest/DG/EX_LookupbyISBN.html<br>
http://api.bnf.fr/

### Geolocalisation :
https://developer.mozilla.org/fr/docs/Web/API/Geolocation_API<br>
https://developers.google.com/maps/documentation/embed/start<br>
https://wiki.openstreetmap.org/wiki/Using_OpenStreetMap#Web_applications<br>
https://www.mapbox.com/

### Scan de code barre :
https://www.npmjs.com/package/react-barcode-reader 

### Technologies envisagées : 
***Front-end :*** ReactJS (notamment pour l’api code barre)<br>
***Back-end :*** NodeJS ou Python/Django ?<br>
***Encapsulation :*** Ionic > Capacitor / Cordova ?

## Structure de données
| Livre                                                                           |
|---------------------------------------------------------------------------------|
| ID<br>SBN<br>Titre<br>Auteur<br>Description<br>Genre<br>Mots-clés<br>Couverture |

| Exemplaire                                                                                                  |
|-------------------------------------------------------------------------------------------------------------|
| ID<br>Livre ID<br>Propriétaire actuel<br>Historique (liste d’événements)<br>Etat (neuf, bon état, abimé...) |

| Utilisateur                                                                                                                                                                 |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ID<br>Nom d’utilisateur<br>Adresse email<br>Mot de passe<br>Localisation<br>Bibliothèque (liste d’exemplaires)<br>Liste de contacts<br>Genres préférés<br>Liste de souhaits |

| Avis                                                |
|-----------------------------------------------------|
| ID<br>Date<br>Auteur<br>Texte<br>Note? (nb étoiles) |

| Messages                                                                  |
|---------------------------------------------------------------------------|
| ID<br>ID Exemplaire<br>Date<br>Emetteur<br>Destinataire<br>Sujet<br>Texte |

| Événement (historique d'un exemplaire)                                                                           |
|------------------------------------------------------------------------------------------------------------------|
| Partage<br>Déménagement<br>Perte<br>Entrée d’un exemplaire dans le réseau<br>Sortie d’un exemplaire du réseau... |