/* Charger les données depuis un fichier externe
Maquette Sprint V1
back : pm2, json-server */
/*----------------------------------------------------------------
                              DATA
----------------------------------------------------------------*/
const books = [{
        title: 'Le messie récalcitrant',
        owner: 'Carole Dupont',
        position: [50.20315813307301, 2.239463167308307]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Pierre Martin',
        position: [48.47097895848431, 7.0967034451110065]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Lucie Pujol',
        position: [47.321944667467655, 5.184577181903621]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Marie Bertrand',
        position: [47.09805038936004, 3.1405801419232886]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Julien Lefevre',
        position: [46.783009968380995, 1.5581308206482092]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Anthony Berteuil',
        position: [45.19545784917763, 0.722949234419681]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Jean Giron',
        position: [43.89578132173855, -0.595758533309565]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Jeanne Ferrand',
        position: [45.70413336672395, -0.5737800705140607]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Gianni Paolini',
        position: [46.39039053249768, -0.5737800705140607]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Zoé Lanvin',
        position: [46.72279205755803, -1.4968755079245468]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Ines Blestel',
        position: [47.752128302937805, -2.6837124988808463]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Ines Blestel',
        position: [46.8167, 1.7]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Ines Blestel',
        position: [46.8, 1.6333]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Ines Blestel',
        position: [46.7667, 1.7667]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Ines Blestel',
        position: [43.8902, -0.4971]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Ines Blestel',
        position: [43.8751, -0.4376]
    },
    {
        title: 'Le messie récalcitrant',
        owner: 'Ines Blestel',
        position: [43.8845, -0.5219]
    }
]

/*----------------------------------------------------------------
                                MAP
----------------------------------------------------------------*/
const mymap = L.map('map').setView([46.31279, 2.4], 6); //France

//MAPBOX
/* L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11', //mapbox/satellite-v9
    accessToken: 'pk.eyJ1IjoiYW50b2luZXBhcmVudCIsImEiOiJjazU5cXkzbWYwMDBpM2xuMW81N2gwbHBsIn0.26rs46belmDrkL_AYeUVHw'
}).addTo(mymap); */

//OPENSTREETMAP
L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
    // Il est toujours bien de laisser le lien vers la source des données
    attribution: '© OpenStreetMap',
    minZoom: 1,
    maxZoom: 20
}).addTo(mymap);

/* const addMarker = (e) => {
    let newlat = e.latlng.lat
    let newlng = e.latlng.lng
    let mess = `[${newlat.toString()},${newlng.toString()}]` //
    console.log(mess)
}
mymap.on('click', addMarker); */

/*----------------------------------------------------------------
                               LIST
----------------------------------------------------------------*/
let markerClusters = L.markerClusterGroup();


const createMarker = (position) => {
    const iconBook = L.icon({
        iconUrl: 'MapMarkerBook-64x64px.png',
        iconSize: [50, 50],
        iconAnchor: [25, 50],
        popupAnchor: [-3, -76]
    })
    const marker = L.marker(position, { icon: iconBook }) //.addTo(mymap);

    marker.bindPopup(`Hello, i'm here :\n ${position}`)
    markerClusters.addLayer(marker);
}

const createItem = (book) => {
    const item = document.createElement('div')
    item.className = 'item'

    const img = document.createElement('img')
    img.setAttribute('src', 'https://via.placeholder.com/400x260')
    item.appendChild(img)

    const title = document.createElement('h4')
    title.innerHTML = book.title
    item.appendChild(title)

    const p = document.createElement('p')
    p.innerHTML = book.owner
    item.appendChild(p)


    createMarker(book.position)

    item.data = book
    item.addEventListener('click', function() {
        mymap.setView(this.data.position, 12) //flyTo
    })

    document.getElementById('list').appendChild(item)
}

books.forEach(book => { createItem(book) })
mymap.addLayer(markerClusters)